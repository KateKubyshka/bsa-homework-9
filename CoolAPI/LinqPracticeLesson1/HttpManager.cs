﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace LinqPracticeLesson1
{
    public static class HttpManager
    {
        private static readonly string uri = "https://localhost:44368/api/";

        private static async Task<T> GetData<T>(string path)
        {
            using HttpClient httpClient = new();
            HttpResponseMessage response = await httpClient.GetAsync(uri + path);
            response.EnsureSuccessStatusCode();
            return JsonConvert.DeserializeObject<T>(await response.Content.ReadAsStringAsync());
        }
        public static async Task<IEnumerable<Project>> GetProjects() => await GetData<IEnumerable<Project>>("projects");
        public static async Task<Project> GetProjectById(int id) => await GetData<Project>($"projects/{id}");
        public static async Task<IEnumerable<ProjectTask>> GetProjectTasks() => await GetData<IEnumerable<ProjectTask>>("tasks");
        public static async Task<ProjectTask> GetProjectTaskById(int id) => await GetData<ProjectTask>($"tasks/{id}");
        public static async Task<IEnumerable<Team>> GetTeams() => await GetData<IEnumerable<Team>>("teams");
        public static async Task<Team> GetTeamById(int id) => await GetData<Team>($"teams/{id}");
        public static async Task<IEnumerable<User>> GetUsers() => await GetData<IEnumerable<User>>("users");
        public static async Task<User> GetUserById(int id) => await GetData<User>($"users/{id}");

        public static async Task SetTaskFinished(int id)
        {
            using HttpClient httpClient = new();
            var response = await httpClient.PatchAsync(uri + $"tasks/{id}", null);
            response.EnsureSuccessStatusCode();
        }

    }
}
