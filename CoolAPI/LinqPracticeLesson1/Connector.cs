﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LinqPracticeLesson1
{
    public class Connector
    {
        public IEnumerable<Project> Data { get; set; }

        public async Task BuildHierarhyAsync()
        {
            IEnumerable<Project> projectsCollection = await HttpManager.GetProjects();
            IEnumerable<ProjectTask> tasksCollection = await HttpManager.GetProjectTasks();
            IEnumerable<Team> teamsCollection = await HttpManager.GetTeams();
            IEnumerable<User> usersCollection = await HttpManager.GetUsers();

            Data = projectsCollection.GroupJoin(
                tasksCollection,
                p => p.Id,
                t => t.ProjectId,
                (proj, tasks) =>
                {
                    proj.Tasks = tasks.Join(
                        usersCollection,
                        t => t.PerformerId,
                        u => u.Id,
                        (task, performer) =>
                        {
                            task.Performer = performer;
                            return task;
                        });

                    return proj;
                })
                .Join(
                usersCollection,
                p => p.AuthorId,
                u => u.Id,
                (proj, user) =>
                {
                    proj.Author = user;
                    return proj;
                })
                .Join(
                teamsCollection,
                p => p.TeamId,
                t => t.Id,
                (proj, team) =>
                {
                    proj.Team = team;
                    return proj;
                });
        }

        public Task<Dictionary<Project, int>> GetTasksByUserId(int id, CancellationToken cancellationToken = default)
        {
            return Task.Run(() => Data.Where(p => p.AuthorId == id)
                       .Select(t => cancellationToken.IsCancellationRequested ? throw new OperationCanceledException(cancellationToken) : t)
                       .Select(p => new { Project = p, Count = p.Tasks.Count() })
                       .ToDictionary(k => k.Project, v => v.Count), cancellationToken);
        }

        public Task<List<ProjectTask>> GetUserTasks(int id, CancellationToken cancellationToken = default)
        {
            return Task.Run(() => Data.SelectMany(p => p.Tasks)
                       .Where(t => t.PerformerId == id && t.Name.Length < 45)
                       .Select(t => cancellationToken.IsCancellationRequested ? throw new OperationCanceledException(cancellationToken) : t)
                       .ToList(), cancellationToken);
        }

        public Task<List<(int Id, string Name)>> GetTaskFinishedIn2021(int id, CancellationToken cancellationToken = default)
        {
            return Task.Run(() => Data.SelectMany(p => p.Tasks)
                       .Select(t => cancellationToken.IsCancellationRequested ? throw new OperationCanceledException(cancellationToken) : t)
                       .Where(t => t.PerformerId == id && t.FinishedAt?.Year == 2021)
                       .Select(t => (t.Id, t.Name))
                       .ToList(), cancellationToken);
        }

        public Task<List<(int Id, string Name, List<User> Users)>> GetTeamsByUsersAge(CancellationToken cancellationToken = default)
        {
            int yearDifference = 10;
            return Task.Run(() => Data.Select(p => p.Tasks.Select(t => t.Performer)
                         .Append(p.Author))
                        .SelectMany(u => u)
                        .Distinct()
                        .Select(t => cancellationToken.IsCancellationRequested ? throw new OperationCanceledException(cancellationToken) : t)
                        .Join(Data.Select(t => t.Team), u => u.TeamId, team => team.Id, (user, team) => new { User = user, Team = team })
                        .OrderByDescending(t => t.User.RegisteredAt)
                        .GroupBy(p => p.Team)
                        .Select(u => (u.Key.Id, u.Key.Name, Users: u.Select(u => u.User).ToList()))
                        .Where(x => x.Users.All(u => DateTime.Now.Year - u.BirthDay.Year > yearDifference))
                        .ToList(), cancellationToken);
        }


        public Task<List<(User User, List<ProjectTask> Tasks)>> GetUserTasks(CancellationToken cancellationToken = default)
        {
            return Task.Run(() => Data.Select(p => p.Tasks.Select(t => t.Performer)
                                        .Append(p.Author))
                                        .SelectMany(u => u)
                                        .Distinct()
                                        .Select(t => cancellationToken.IsCancellationRequested ? throw new OperationCanceledException(cancellationToken) : t)
                                        .OrderBy(p => p.FirstName)
                            .GroupJoin(Data.SelectMany(p => p.Tasks),
                                p => p.Id,
                                t => t.PerformerId,
                                (user, tasks) => (user, tasks.OrderByDescending(t => t.Name.Length)
                                                              .ToList()))
                                                              .ToList(), cancellationToken);
        }



        public Task<UserClass> GetUserInfo(int id, CancellationToken cancellationToken = default)
        {
            return Task.Run(() => Data.Select(p => p.Tasks.Select(t => t.Performer)
                                     .Append(p.Author))
                                     .SelectMany(u => u)
                                     .Distinct()
                                     .Select(t => cancellationToken.IsCancellationRequested ? throw new OperationCanceledException(cancellationToken) : t)
                         .GroupJoin(
                             Data,
                             u => u.Id,
                             p => p.AuthorId,
                             (user, projects) => new
                             {
                                 LastProject = projects.OrderByDescending(p => p.CreatedAt).FirstOrDefault(),
                                 User = user
                             })
                         .GroupJoin(
                               Data.SelectMany(p => p.Tasks),
                               u => u.User.Id,
                               t => t.PerformerId,
                               (user, tasks) => new UserClass()
                               {
                                   LastProject = user.LastProject,
                                   CountOfTasks = tasks?.Count(),
                                   AmountOfUnfinishedTasks = tasks?.Where(t => t.FinishedAt is null).Count(),
                                   TheLongestTask = tasks?
                                                    .Where(t => t.FinishedAt is not null)
                                                    .Select(t => new { Task = t, Duration = (t.FinishedAt - t.CreatedAt) })
                                                    .OrderByDescending(t => t.Duration).First().Task
                               }).FirstOrDefault(u => u.User.Id == id), cancellationToken);
        }

        public Task<IEnumerable<ProjectClass>> GetProjects(CancellationToken cancellationToken = default)
        {
            return Task.Run(() => Data
                .Select(t => cancellationToken.IsCancellationRequested ? throw new OperationCanceledException(cancellationToken) : t)
                .Select(p => new
                {
                    Project = p,
                    LongestTask = p.Tasks?.OrderByDescending(t => t.Description.Length).FirstOrDefault(),
                    ShortestTask = p.Tasks?.OrderBy(t => t.Name).FirstOrDefault()
                })
                .GroupJoin(Data.Select(p => p.Tasks.Select(t => t.Performer).Append(p.Author))
                            .SelectMany(u => u)
                            .Distinct(),
                            p => p.Project.TeamId,
                            u => u.TeamId,
                            (proj, users) => new ProjectClass
                            {
                                Project = proj.Project,
                                LongestTask = proj.LongestTask,
                                ShortestTask = proj.ShortestTask,
                                AmountOfMembers = users.Count()
                            })
                .Where(p => p.Project.Description.Length > 20 || p.Project.Tasks.Count() < 3), cancellationToken);
        }

        public Task<int> MarkRandomTaskWithDelay(int delay, CancellationToken cancellationToken = default)
        {
            var tcs = new TaskCompletionSource<int>();

            var backgroundWorker = new BackgroundWorker();
            var random = new Random();

            backgroundWorker.DoWork += (o, e) =>
            {
                Thread.Sleep(delay);

                IEnumerable<ProjectTask> tasksCollection = HttpManager.GetProjectTasks().Result;

                var unfinishedTasks = tasksCollection.Where(t => !t.FinishedAt.HasValue).ToList();
                if (unfinishedTasks.Count == 0)
                {
                    throw new InvalidOperationException("There is no unfinished tasks anymore");
                }
                var randomId = unfinishedTasks[random.Next(unfinishedTasks.Count)].Id;

                HttpManager.SetTaskFinished(randomId).Wait();

                e.Result = randomId;
            };

            backgroundWorker.RunWorkerCompleted += (o, e) =>
            {
                if (e.Error is not null)
                {
                    tcs.SetException(e.Error);
                }
                else
                {
                    tcs.SetResult((int)e.Result);
                }
            };

            backgroundWorker.RunWorkerAsync();

            return tcs.Task;
        }
    }
}
