﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LinqPracticeLesson1
{
    class Program
    {
        private static Connector connector = new();
        static async Task Main(string[] args)
        {
            try
            {
                bool IsContinue = true;
                await connector.BuildHierarhyAsync();
                while (IsContinue)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("Choose from the menu and enter your number");
                    Console.ForegroundColor = ConsoleColor.DarkGray;
                    PrintMenu();
                   
                    int input = int.Parse(Console.ReadLine());
                    switch (input)
                    {
                        case 1:
                            GetTasksAsync();
                            break;
                        case 2:
                            GetUserTasks();
                            break;
                        case 3:
                            GetUnfinishedTasks();
                            break;
                        case 4:
                            GetListFromTeams();
                            break;
                        case 5:
                            GetSortedUsers();
                            break;
                        case 6:
                            GetUsersStructure();
                            break;
                        case 7:
                            GetProjectStructure();
                            break;
                        case 8:
                            StartTimer();
                            break;
                        case 9:
                            StopTimer();
                            break;
                        default:
                            break;
                    }
                    
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Something went wrong");
                Console.WriteLine(ex.Message);
            }
}

        private static CancellationTokenSource cancellationTokenSource;
        private static void StopTimer()
        {
            cancellationTokenSource?.Cancel();
        }
        private static void StartTimer()
        {
            cancellationTokenSource?.Cancel(); 
            cancellationTokenSource = new CancellationTokenSource();
            var cancellationToken = cancellationTokenSource.Token;
            int delay = 1000;
            System.Timers.Timer timer = new(delay);
            timer.Elapsed += async (o, e) =>
            {
                try
                {
                    if (cancellationToken.IsCancellationRequested)
                    {
                        timer.Stop();
                    }
                    else
                    {
                        var id = await connector.MarkRandomTaskWithDelay(delay, cancellationToken);
                        Console.WriteLine("Id = " + id);
                    }
                }
                catch (Exception)
                {
                    timer.Stop();
                    Console.WriteLine("Timer was stopped");
                }
            };
            timer.Start();
        }

        private static async Task GetProjectStructure()
        {
            try
            {
                IEnumerable<ProjectClass> projects = await connector.GetProjects();
                if (projects is null) Console.WriteLine("There is no projects");
                else
                {
                    foreach (var project in projects)
                    {
                        Console.WriteLine("Project");
                        Console.WriteLine($"Id : {project.Project.Id}\tName : {project.Project.Name} \tCreated : {project.Project.CreatedAt}");
                        if (!project.Project.Tasks.Any()) Console.WriteLine("There is no tasks in this project");
                        else
                        {
                            Console.WriteLine($"Longest task Id : {project.LongestTask.Id}\tName : {project.LongestTask.Name}\t Performer : {project.LongestTask.Performer.FirstName}");
                            Console.WriteLine($"Shortest task Id : {project.ShortestTask.Id}\t Name : {project.ShortestTask.Name}\t Performer : {project.ShortestTask.Performer.FirstName}");
                            Console.WriteLine($"Amount of members in project : {project.AmountOfMembers}");
                        }
                       
                    }
                }
            }
            catch (ArgumentNullException ex)
            {
                Console.WriteLine("somthing went wrong");
                Console.WriteLine(ex.Message);
            }
        }

        private static async Task GetUsersStructure()
        {
            Console.WriteLine("Enter a user`s ID");
            try
            {
                int id = int.Parse(Console.ReadLine());
                UserClass userInfo = await connector.GetUserInfo(id);
                if (userInfo is null) Console.WriteLine("Can`t get userInfo");
                else
                {
                    Console.WriteLine("User : ");
                    Console.WriteLine($"User Id : {userInfo.User.Id}\t User Name : {userInfo.User.FirstName}\t {userInfo.User.LastName}");
                    Console.WriteLine($"Last user`s project Id : {userInfo.LastProject.Id} \t Project Name : {userInfo.LastProject.Name} \t Created : {userInfo.LastProject.CreatedAt}");
                    Console.WriteLine($"Amount of tasks in the last project : {userInfo.CountOfTasks}");
                    Console.WriteLine($"Amount of unfinished tasks : {userInfo.AmountOfUnfinishedTasks}");
                    Console.WriteLine($"The longest task Id: {userInfo.TheLongestTask.Id}\t Name : {userInfo.TheLongestTask.Name} \t State : {userInfo.TheLongestTask.State}");
                }
            }
            catch (ArgumentNullException ex)
            {
                Console.WriteLine("somthing went wrong");
                Console.WriteLine(ex.Message);
            }
            catch (FormatException)
            {
                Console.WriteLine("Incorrect input! Right format of an ID is a natural number");
            }

        }

        private static async Task GetSortedUsers()
        {
            try
            {
                List<(User User, List<ProjectTask> Tasks)> userTasks =await connector.GetUserTasks();
                if (userTasks.Count == 0) Console.WriteLine("There is no users or tasks");
                else
                {
                    foreach (var item in userTasks)
                    {
                        Console.WriteLine($"User Id : {item.User.Id} \tUser Name : {item.User.FirstName}");
                        foreach (var task in item.Tasks)
                        {
                            Console.WriteLine($"Task Name : {task.Name}\t Task Name Length : {task.Name.Length}");
                        }
                    }
                }
            }
            catch (ArgumentNullException ex)
            {
                Console.WriteLine("somthing went wrong");
                Console.WriteLine(ex.Message);
            }
            catch (FormatException)
            {
                Console.WriteLine("Incorrect input! Right format of an ID is a natural number");
            }


        }

        private static async Task GetListFromTeams()
        {
            try
            {
                List<(int Id, string Name, List<User> Users)> usersList = await connector.GetTeamsByUsersAge();
                if (usersList.Count == 0) Console.WriteLine("There is no members");
                else
                {
                    foreach (var item in usersList)
                    {
                        Console.WriteLine($" Id : {item.Id} \t Team Name : {item.Name}");
                        foreach (var user in item.Users)
                        {
                            Console.WriteLine($"\t\tUserId : {user.Id} \t User Name : {user.FirstName} \t {user.LastName} \t teamId : {user.TeamId} ");
                        }
                    }
                }
            }
            catch (ArgumentNullException ex)
            {
                Console.WriteLine("somthing went wrong");
                Console.WriteLine(ex.Message);
            }
        }

        private static async Task GetUnfinishedTasks()
        {
            Console.WriteLine("Enter a user`s ID");
            try
            {
                int id = int.Parse(Console.ReadLine());
                List<(int id, string name)> unfinishedTasks = await connector.GetTaskFinishedIn2021(id);
                if (unfinishedTasks.Count == 0) Console.WriteLine($"There is no unfinished tasks of user {id}");
                else
                {
                    Console.WriteLine("Unfinished tasks in 2021 :");
                    foreach (var item in unfinishedTasks)
                    {
                        Console.WriteLine($"Task Id : {item.id} \t Name : {item.name}");
                    }
                }
            }
            catch (ArgumentNullException ex)
            {
                Console.WriteLine("somthing went wrong");
                Console.WriteLine(ex.Message);
            }
            catch (FormatException)
            {
                Console.WriteLine("Incorrect input! Right format of an ID is a natural number");
            }
        }

        private static async Task GetUserTasks()
        {
            Console.WriteLine("Enter a user`s ID");
            try
            {
                int id = int.Parse(Console.ReadLine());
                List<ProjectTask> projectTasks = await connector.GetUserTasks(id);
                if (projectTasks.Count == 0) Console.WriteLine("User was not found, try another");
                foreach (var item in projectTasks)
                {
                    Console.WriteLine($"User id : {item.Performer.Id} \t Task name : {item.Name}");
                }

            }
            catch (ArgumentNullException ex)
            {
                Console.WriteLine("somthing went wrong");
                Console.WriteLine(ex.Message);
            }
            catch (FormatException)
            {
                Console.WriteLine("Incorrect input! Right format of an ID is a natural number");
            }
        }

        private static async Task GetTasksAsync()
        {
            Console.WriteLine("Enter a user`s ID");
            try
            {
                int id = int.Parse(Console.ReadLine());
                Dictionary<Project, int> tasks = await connector.GetTasksByUserId(id);
                if (tasks.Count == 0) Console.WriteLine("User was not found, try another");
                foreach (var item in tasks)
                {
                    Console.WriteLine($"Project name :  {item.Key.Name} \t Amount of tasks : {item.Value}");
                }
            }
            catch(ArgumentNullException ex)
            {
                Console.WriteLine("somthing went wrong");
                Console.WriteLine(ex.Message);
            }
            catch (FormatException)
            {
                Console.WriteLine("Incorrect input! Right format of an ID is a natural number");
            }
        }

        private static void PrintMenu()
        {
            Console.WriteLine("---------------------------------------------");
            Console.WriteLine("1. Get amount of tasks of a user");
            Console.WriteLine("2. Get user`s tasks with task name length less than 45 symbols");
            Console.WriteLine("3. Get finished tasks of a user");
            Console.WriteLine("4. Get the list of teams in which members` age greater than 10");
            Console.WriteLine("5. Get sorted list of members");
            Console.WriteLine("6. Get a Users structure");
            Console.WriteLine("7. Get a Project structure");
            Console.WriteLine("8. Set timer and start finishing tasks");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("\nEnter necessary number");
        }
    }
}