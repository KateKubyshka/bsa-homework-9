﻿using AutoMapper;
using DAL.Entities;
using DAL.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebAPI.DTOs;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private readonly IGenericRepository<Team> _repository;
        private readonly IMapper _mapper;

        public TeamsController(IGenericRepository<Team> repository, IMapper mapper) => (_repository, _mapper) = (repository, mapper);


        [HttpGet]
        public async Task<ActionResult<IEnumerable<TeamDTO>>> Get()
        {
            return Ok(_mapper.Map<IEnumerable<TeamDTO>>(await _repository.GetAllAsync()));
        }


        [HttpGet("{id}")]
        public async Task<ActionResult<TeamDTO>> Get(int id)
        {
            try
            {
                return Ok(_mapper.Map<TeamDTO>(await _repository.GetByIdAsync(id)));
            }
            catch (KeyNotFoundException)
            {
                return NotFound();
            }
        }

        [HttpPost]
        public async Task<ActionResult> Post([FromBody] TeamDTO team)
        {
            try
            {
                await _repository.UpdateAsync(_mapper.Map<Team>(team));
                return Ok();
            }
            catch (ArgumentException)
            {
                return BadRequest();
            }
            catch (KeyNotFoundException)
            {
                return NotFound();
            }
            catch (DbUpdateException)
            {
                return BadRequest();
            }
        }


        [HttpPut]
        public async Task<ActionResult> Put([FromBody] TeamDTO team)
        {
            try
            {
               await  _repository.InsertAsync(_mapper.Map<Team>(team));
                return Ok();
            }
            catch (ArgumentException)
            {
                return BadRequest();
            }
            catch (DbUpdateException)
            {
                return BadRequest();
            }
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                await _repository.DeleteAsync(id);
                return Ok();
            }
            catch (KeyNotFoundException)
            {
                return NotFound();
            }
            catch (DbUpdateException)
            {
                return BadRequest();
            }
        }
    }
}
