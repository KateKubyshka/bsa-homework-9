﻿using AutoMapper;
using DAL.Entities;
using DAL.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebAPI.DTOs;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly IGenericRepository<ProjectTask> _repository;
        private readonly IMapper _mapper;

        public TasksController(IGenericRepository<ProjectTask> repository, IMapper mapper) => (_repository, _mapper) = (repository, mapper);

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ProjectTaskDTO>>> Get()
        {
            return Ok(_mapper.Map<IEnumerable<ProjectTaskDTO>>(await _repository.GetAllAsync()));
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ProjectTaskDTO>> Get(int id)
        {
            try
            {
                return Ok(_mapper.Map<ProjectTaskDTO>(await _repository.GetByIdAsync(id)));
            }
            catch (KeyNotFoundException)
            {
                return NotFound();
            }
        }

        [HttpPost]
        public async Task<ActionResult> Post([FromBody] ProjectTaskDTO task)
        {
            try
            {
                await _repository.UpdateAsync(_mapper.Map<ProjectTask>(task));
                return Ok();
            }
            catch (ArgumentException)
            {
                return BadRequest();
            }
            catch (KeyNotFoundException)
            {
                return NotFound();
            }
            catch (DbUpdateException)
            {
                return BadRequest();
            }
        }

        [HttpPut]
        public async Task<ActionResult> Put([FromBody] ProjectTaskDTO task)
        {
            try
            {
                await _repository.InsertAsync(_mapper.Map<ProjectTask>(task));
                return Ok();
            }
            catch (ArgumentException)
            {
                return Conflict();
            }
            catch (DbUpdateException)
            {
                return BadRequest();
            }
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                await _repository.DeleteAsync(id);
                return Ok();
            }
            catch (KeyNotFoundException)
            {
                return NotFound();
            }
            catch (DbUpdateException)
            {
                return BadRequest();
            }
        }

        [HttpPatch("{id}")]
        public async Task<ActionResult<int>> FinishTaskAsync(int id)
        {
            try
            {
                var entity = await _repository.GetByIdAsync(id) ?? throw new KeyNotFoundException();
                entity.FinishedAt = DateTime.Now;
                await _repository.UpdateAsync(entity);
                return Ok(id);
            }
            catch (Exception)
            {
                return NotFound();
            }
           
        }

    }
}
