using DAL;
using DAL.Entities;
using DAL.Interfaces;
using DAL.Repositories;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using WebAPI.Interfaces;
using WebAPI.Parsers;

namespace WebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddControllers();

            services.AddTransient<IParser, Parser>();
            services.AddDbContext<ProjectDbContext>(options => options.UseSqlServer(Configuration.GetConnectionString("ProjectDatabase")));

            services.AddScoped<IGenericRepository<Project>, EFRepository<Project>>();
            services.AddScoped<IGenericRepository<ProjectTask>, EFRepository<ProjectTask>>();
            services.AddScoped<IGenericRepository<Team>, EFRepository<Team>>();
            services.AddScoped<IGenericRepository<User>, EFRepository<User>>();
           

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "WebAPI", Version = "v1" });
            });
            services.AddAutoMapper(typeof(Startup));


        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "WebAPI v1"));
            }

            app.UseHttpsRedirection();
            app.UseCors(builder => builder
                .AllowAnyMethod()
                .AllowAnyHeader()
                .AllowCredentials()
                .WithOrigins("http://localhost:4200"));
            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
            Seed(app);
        }

        private void Seed(IApplicationBuilder app)
        {
            var serviceProvider = app.ApplicationServices;
            var parser = serviceProvider.GetService<IParser>();


            using var serviceScope = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope();
            var projects = serviceScope.ServiceProvider.GetService<IGenericRepository<Project>>();
            var projectTasks = serviceScope.ServiceProvider.GetService<IGenericRepository<ProjectTask>>();
            var teams = serviceScope.ServiceProvider.GetService<IGenericRepository<Team>>();
            var users = serviceScope.ServiceProvider.GetService<IGenericRepository<User>>();
            if (!teams.Any())
            {
                foreach (var item in parser.Parse<Team>(Configuration["Paths:Teams"]))
                {
                    teams.Insert(item);
                }
            }
            if (!users.Any())
            {
                foreach (var item in parser.Parse<User>(Configuration["Paths:Users"]))
                {
                    users.Insert(item);
                }
            }
            if (!projects.Any())
            {
                foreach (var item in parser.Parse<Project>(Configuration["Paths:Projects"]))
                {
                    projects.Insert(item);
                }
            }
            if (!projectTasks.Any())
            {
                foreach (var item in parser.Parse<ProjectTask>(Configuration["Paths:ProjectsTasks"]))
                {
                    projectTasks.Insert(item);
                }
            }
        }
    }
}
