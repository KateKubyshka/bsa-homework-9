﻿using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace DAL.Interfaces
{
    public interface IGenericRepository<T> where T : Entity
    {
        bool Any();
        IEnumerable<T> GetAll();
        Task<IEnumerable<T>> GetAllAsync(CancellationToken cancellationToken = default);
        T GetById(int id);
        Task<T> GetByIdAsync(int id, CancellationToken cancellationToken = default);
        void Insert(T obj);
        Task InsertAsync(T obj, CancellationToken cancellationToken = default);
        void Update(T obj);
        Task UpdateAsync(T obj, CancellationToken cancellationToken = default);
        void Delete(int id);
        Task DeleteAsync(int id, CancellationToken cancellationToken = default);
    }
}
