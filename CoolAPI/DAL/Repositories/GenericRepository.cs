﻿using DAL.Entities;
using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class GenericRepository<T> : IGenericRepository<T> where T :  Entity
    {
        private List<T> _context = new();
        public bool Any() => _context.Any();
    
        public void Delete(int id)
        {
            var entity = _context.FirstOrDefault(i => i.Id == id);
            if (entity is not null)
            {
                _context.Remove(entity);
            }
            else
            {
                throw new KeyNotFoundException();
            }
        }

        public Task DeleteAsync(int id, CancellationToken cancellationToken = default) => Task.Run(() => Delete(id), cancellationToken);

        public IEnumerable<T> GetAll()
        {
            return _context;
        }

        public Task<IEnumerable<T>> GetAllAsync(CancellationToken cancellationToken = default) => Task.Run(GetAll, cancellationToken);

        public T GetById(int id)
        {
            var entity = _context.FirstOrDefault(i => i.Id == id);
            if(entity is not null)
            {
                return entity;
            }
            else
            {
                throw new KeyNotFoundException();
            }
           
        }

        public Task<T> GetByIdAsync(int id, CancellationToken cancellationToken = default) => Task.Run(() => GetById(id), cancellationToken);

        public void Insert(T obj)
        {
            if (obj is null)
            {
                throw new ArgumentNullException(nameof(obj));
            }
            if(_context.Any(i=> i.Id == obj.Id))
            {
                throw new ArgumentException(nameof(obj));
            }
            _context.Add(obj);
        }

        public Task InsertAsync(T obj, CancellationToken cancellationToken = default) => Task.Run(() => Insert(obj), cancellationToken);

        public void Update(T obj)
        {
            if (obj is null)
            {
                throw new ArgumentNullException(nameof(obj));
            }
            var entity = _context.FirstOrDefault(i => i.Id == obj.Id);
            if (entity is not null)
            {
                _context.Remove(entity);
                _context.Add(obj);
            }
            else
            {
                throw new KeyNotFoundException();
            }
        }

        public Task UpdateAsync(T obj, CancellationToken cancellationToken = default) => Task.Run(() => Update(obj), cancellationToken);
    }
}
