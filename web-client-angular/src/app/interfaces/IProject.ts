export interface IProject {
    id: number,
    teamId: number,
    authorId: number,
    description: string,
    name: string,
    deadline: Date
}