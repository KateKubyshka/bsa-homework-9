import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { ProjectsModule } from './projects/projects.module';
import { AppRoutingModule } from './app-routing.module';
import { ProjectTasksModule } from './project-tasks/project-tasks.module';
import { TeamsModule } from './teams/teams.module';
import { UsersModule } from './users/users.module';
import { HomeComponent } from './home/home.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DateFormatUAModule } from './date-format-ua/date-format-ua.module';
import { CanDeactivateGuard } from './services/can-deactivate-guard.service';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    ProjectsModule,
    ProjectTasksModule,
    TeamsModule,
    UsersModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    DateFormatUAModule,
    ReactiveFormsModule,
  ],
  providers: [CanDeactivateGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
