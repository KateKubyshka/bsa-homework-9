import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TasksComponent } from './project-tasks/tasks/tasks.component';
import { ProjectsEditComponent } from './projects/projects-edit/projects-edit.component';
import { ProjectsComponent } from './projects/projects/projects.component';
import { TeamsComponent } from './teams/teams/teams.component';
import { UsersComponent } from './users/users/users.component';
import { CanDeactivateGuard } from './services/can-deactivate-guard.service';

const routes: Routes = [
  { path: 'projects', component: ProjectsComponent },
  { path: 'projects/edit/:id', component: ProjectsEditComponent, canDeactivate: [CanDeactivateGuard]},
  { path: 'tasks', component: TasksComponent },
  { path: 'teams', component: TeamsComponent },
  { path: 'users', component: UsersComponent },
  { path: '**', redirectTo: 'projects'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }