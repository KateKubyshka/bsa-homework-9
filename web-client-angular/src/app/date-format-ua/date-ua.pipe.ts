import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dateUA'
})
export class DateUAPipe implements PipeTransform {

  transform(dateIn: Date | null | string): string {
    if(dateIn === null){
      return "";
    }

    const date = typeof dateIn === 'string' ? new Date(dateIn) : dateIn;
    
    const day : number = date.getDate();
    const month : number = date.getMonth();
    let monthStr : string;
    switch(month){
      case 0:
        monthStr = "січня";
        break;
      case 1:
        monthStr = "лютого";
        break;
      case 2: 
        monthStr = "березня";
        break;
      case 3:
        monthStr = "квітня";
        break;
      case 4:
        monthStr = "травня";
        break;
      case 5:
        monthStr = "червня";
        break;
      case 6 :
        monthStr = "липня";
        break;
      case 7:
        monthStr = "серпня";
        break;
      case 8 :
        monthStr = "вересня";
        break;
      case 9:
        monthStr = "жовтня";
        break;
      case 10: 
        monthStr = "листопада";
        break;
      case 11:
        monthStr = "грудня";
        break;
      default:
        monthStr = "мартабря";
        break;
    }
    const year = date.getFullYear();
    return day + " " + monthStr + " " + year;

  }

}