import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DateUAPipe } from './date-ua.pipe';


@NgModule({
  declarations: [
    DateUAPipe
  ],
  imports: [
    CommonModule
  ],
  exports: [
    DateUAPipe
  ]
})
export class DateFormatUAModule { }
