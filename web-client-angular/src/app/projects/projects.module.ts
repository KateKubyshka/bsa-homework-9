import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectsComponent } from './projects/projects.component';
import { DateFormatUAModule } from '../date-format-ua/date-format-ua.module';
import { ProjectsEditComponent } from './projects-edit/projects-edit.component';
import { AppRoutingModule } from '../app-routing.module';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    ProjectsComponent,
    ProjectsEditComponent
  ],
  imports: [
    CommonModule,
    DateFormatUAModule,
    AppRoutingModule,
    FormsModule
  ],
  exports : [
    ProjectsComponent,
    ProjectsEditComponent
  ]
})
export class ProjectsModule { }
