import { Injectable } from '@angular/core';
import { IProject } from 'src/app/interfaces/IProject';
import { HttpInternalService } from 'src/app/services/http-internal.service';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {
  
  private httpService: HttpInternalService;

  constructor(httpService: HttpInternalService) {
    this.httpService = httpService;
   }
  
   public getAllProjects(){
    return this.httpService.getRequest<IProject[]>('api/projects');
   }

   public getProjectById(id: number){
     return this.httpService.getRequest<IProject>(`api/projects/${id}`);
   }

   public updateProject(project :IProject){
      return this.httpService.postFullRequest<IProject>('api/projects', project);
   }
}
