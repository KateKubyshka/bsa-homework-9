import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProjectService } from '../services/project.service';
import { IProject } from 'src/app/interfaces/IProject';
import { Observable, of } from 'rxjs';

@Component({
  selector: 'app-projects-edit',
  templateUrl: './projects-edit.component.html',
  styleUrls: ['./projects-edit.component.css']
})
export class ProjectsEditComponent implements OnInit {
  public id : number | undefined;
  public project : IProject = {} as IProject;
  constructor(
    private route: ActivatedRoute,
    private projectsService : ProjectService,
  ) {}

  unSaved: boolean = false;        

  canDeactivate(): Observable<boolean> | boolean {
      if (this.unSaved) {

        const result = window.confirm('You have unsaved changes! Do you want to quit?');

         return of(result);
      }
      return true;
  }   



  ngOnInit(): void {
    this.id = Number(this.route.snapshot.paramMap.get('id'));
    this.projectsService.getProjectById(this.id).subscribe(project => {
        this.project = project;
      });

  }

  saveChanges(){
    if (this.project) {
      this.projectsService.updateProject(this.project)
      .subscribe(response => {
        if (response.ok) {
          this.unSaved = false;
          alert('saved');
        }
        else {
          alert(`response code: ${response.status}`);
        }
      },
      error => console.error(error));
    }

  }
  parseDate(event: Event): Date {
    return new Date((event.target as HTMLInputElement).value);
}

}
