import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IProject } from 'src/app/interfaces/IProject';
import { ProjectService } from '../services/project.service';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css']
})
export class ProjectsComponent implements OnInit {

  projects: IProject[] = []; 
  constructor(private projectsService: ProjectService) {}

  ngOnInit(): void {
    this.projectsService.getAllProjects().subscribe((projects) => this.projects = projects);
  }

}
